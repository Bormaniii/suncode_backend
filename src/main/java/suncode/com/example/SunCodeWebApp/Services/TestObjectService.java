package suncode.com.example.SunCodeWebApp.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import suncode.com.example.SunCodeWebApp.DAO.api.ITestObjectDAO;
import suncode.com.example.SunCodeWebApp.Models.OccurrenceEnum;
import suncode.com.example.SunCodeWebApp.Models.TestObject;

import java.util.List;

@Service
public class TestObjectService {

    @Autowired
    private ITestObjectDAO iTestObjectDAO;

    public List<TestObject> getDuplicatesByColumnName(String columnName) {
        return iTestObjectDAO.findOccurrenceByColumn(columnName, OccurrenceEnum.DUPLICATES);
    }

    public List<TestObject> getUniquesByColumnName(String columnName) {
        return iTestObjectDAO.findOccurrenceByColumn(columnName, OccurrenceEnum.EQUAL);
    }
}
