package suncode.com.example.SunCodeWebApp.Controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import suncode.com.example.SunCodeWebApp.Models.TestObject;
import suncode.com.example.SunCodeWebApp.Services.TestObjectService;

import java.util.List;

@RestController
@RequestMapping("/testObject")
public class TestObjectController {
    private final TestObjectService testObjectService;

    public TestObjectController(TestObjectService testObjectService) {
        this.testObjectService = testObjectService;
    }

    @GetMapping("/duplicates/{column}")
    public ResponseEntity<List<TestObject>> getDuplicatesByColumn(@PathVariable("column") String column) {
        List<TestObject> testObjects = testObjectService.getDuplicatesByColumnName(column);
        return new ResponseEntity<>(testObjects, HttpStatus.OK);
    }

    @GetMapping("/uniques/{column}")
    public ResponseEntity<List<TestObject>> getUniquesByColumn(@PathVariable("column") String column) {
        List<TestObject> testObjects = testObjectService.getUniquesByColumnName(column);
        return new ResponseEntity<>(testObjects, testObjects != null ? HttpStatus.OK : HttpStatus.NO_CONTENT);
    }
}
