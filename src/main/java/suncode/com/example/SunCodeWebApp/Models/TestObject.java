package suncode.com.example.SunCodeWebApp.Models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "tabela_testowa")
public class TestObject implements Serializable {
    @Id
    @Column(nullable = false)
    @Getter
    @Setter
    private Long Id;
    @Getter
    @Setter
    private String kolumna1;
    @Getter
    @Setter
    private String kolumna2;
    @Getter
    @Setter
    private String kolumna3;
    @Getter
    @Setter
    private Long kolumna4;

    public TestObject() {
    }

    public TestObject(String kolumna1, String kolumna2, String kolumna3, Long kolumna4) {
        this.kolumna1 = kolumna1;
        this.kolumna2 = kolumna2;
        this.kolumna3 = kolumna3;
        this.kolumna4 = kolumna4;
    }
}
