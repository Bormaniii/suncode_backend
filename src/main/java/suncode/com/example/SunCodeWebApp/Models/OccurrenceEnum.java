package suncode.com.example.SunCodeWebApp.Models;

public enum OccurrenceEnum {
    DUPLICATES(">"),
    EQUAL("=");

    private String occurrence;

    OccurrenceEnum(String occurrence) {
        this.occurrence = occurrence;
    }

    @Override
    public String toString() {
        return this.occurrence;
    }
}
