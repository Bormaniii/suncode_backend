package suncode.com.example.SunCodeWebApp.DAO.api;

import suncode.com.example.SunCodeWebApp.Models.OccurrenceEnum;
import suncode.com.example.SunCodeWebApp.Models.TestObject;

import java.util.List;


public interface ITestObjectDAO {

    List<TestObject> findOccurrenceByColumn(String columnName, OccurrenceEnum occurrenceEnum);
}
