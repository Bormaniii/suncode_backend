package suncode.com.example.SunCodeWebApp.DAO.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import suncode.com.example.SunCodeWebApp.DAO.HibernateUtil;
import suncode.com.example.SunCodeWebApp.DAO.api.ITestObjectDAO;
import suncode.com.example.SunCodeWebApp.Models.OccurrenceEnum;
import suncode.com.example.SunCodeWebApp.Models.TestObject;

import java.io.Serializable;
import java.util.List;

@Repository
public class TestObjectDAO<T extends Serializable> implements ITestObjectDAO {

    private final String occurrenceQuery = "FROM TestObject to1 WHERE to1.%1$s in ( " +
            "SELECT to2.%1$s FROM TestObject to2 " +
            "GROUP BY to2.%1$s " +
            "HAVING count(to2.%1$s) %2$s 1) " +
            "ORDER BY to1.id";

    public List<TestObject> findOccurrenceByColumn(String columnName, OccurrenceEnum occurrenceEnum) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        final String query = String.format(occurrenceQuery, columnName, occurrenceEnum.toString());
        List<TestObject> testObjects = null;

        try {
            session.beginTransaction();
            testObjects = session.createQuery(query).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return testObjects;
    }
}
