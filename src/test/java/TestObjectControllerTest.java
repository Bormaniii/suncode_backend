import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import suncode.com.example.SunCodeWebApp.Models.TestObject;
import suncode.com.example.SunCodeWebApp.SunCodeWebAppApplication;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = SunCodeWebAppApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-test.properties")
public class TestObjectControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @Sql("/baza_danych_testowa.sql")
    public void testGetDuplicatesByColumnNameKolumna1() throws Exception {
        List<TestObject> testObjects;
        mvc.perform(get("/testObject/duplicates/kolumna1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    @Sql("/baza_danych_testowa.sql")
    public void testGetDuplicatesByColumnNameWrongName() throws Exception {
        List<TestObject> testObjects;
        mvc.perform(get("/testObject/uniques/kolumna5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    @Sql("/baza_danych_testowa.sql")
    public void testGetUniquesByColumnNameKolumna1() throws Exception {
        List<TestObject> testObjects;
        mvc.perform(get("/testObject/uniques/kolumna1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    @Sql("/baza_danych_testowa.sql")
    public void testGetUniquesByColumnNameWrongName() throws Exception {
        List<TestObject> testObjects;
        mvc.perform(get("/testObject/uniques/kolumna5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}
